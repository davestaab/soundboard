#!/usr/bin/python
import pygame.mixer
from time import sleep
import RPi.GPIO as GPIO
import json
from sys import exit
import sys


# Hack for now
# sound_config = None

# sounds should just be the file, not the directory
soundDir = "sounds/";

def get_sound_file(button_input):
    # This allows us to only open the config file when a button is pressed
    sound_config_file = open("sounds.json", "r")
    sound_config = json.load(sound_config_file)
    sound_config_file.close()

    input_index = "input_" + button_input
    counter = sound_config[input_index]["counter"]
    sound_file = soundDir + sound_config[input_index]["files"][counter]

    if len(sound_config[input_index]["files"]) > 1:
        counter += 1
        if counter >= len(sound_config[input_index]["files"]):
            counter = 0
        sound_config[input_index]["counter"] = counter

    print sound_file

    sound_config_file = open("sounds.json", "w")
    json.dump(sound_config, sound_config_file, sort_keys=True, indent=4, separators=(',', ': '))
    sound_config_file.close()

    return pygame.mixer.Sound(sound_file)

def play_sound(channel):
    soundChannelA.play(get_sound_file(str(channel)))


# The main
if __name__ == '__main__':

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(23, GPIO.IN)
    GPIO.setup(24, GPIO.IN)
    GPIO.setup(25, GPIO.IN)
    GPIO.setup(18, GPIO.IN)
    GPIO.setup(4, GPIO.IN)
    GPIO.setup(17, GPIO.IN)
    GPIO.setup(21, GPIO.IN)
    GPIO.setup(22, GPIO.IN)

    pygame.mixer.init(48000, -16, 1, 1024)

    soundChannelA = pygame.mixer.Channel(1)
    soundChannelB = pygame.mixer.Channel(2)
    soundChannelC = pygame.mixer.Channel(3)
    soundChannelD = pygame.mixer.Channel(4)
    soundChannelE = pygame.mixer.Channel(5)
    soundChannelF = pygame.mixer.Channel(6)
    soundChannelG = pygame.mixer.Channel(7)
    soundChannelH = pygame.mixer.Channel(0)

    print "Sampler Ready."

    GPIO.add_event_detect(23, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(24, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(25, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(18, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(4, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(17, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(21, GPIO.RISING, callback=play_sound, bouncetime=200)
    GPIO.add_event_detect(22, GPIO.RISING, callback=play_sound, bouncetime=200)

    try:
        if len(sys.argv) > 1:
            sys.stdin = open('/dev/null')
        command = raw_input("Hit enter to exit...")
    except KeyboardInterrupt:
        exit()
