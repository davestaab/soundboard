'use strict';

describe('orderObjectBy filter', function () {

  beforeEach(module('soundboard'));

  var orderObjectBy;

  beforeEach(inject(function ($filter) {
    orderObjectBy = $filter('orderObjectBy');
  }));

  it('should sort object by properties', function () {
    var obj = {
      key1: {
        button: 3
      },
      key2: {
        button: 2
      },
      key3: {
        button: 1
      }
    };
    expect(orderObjectBy(obj)).toEqual([{button: 1, key: 'key3'}, {button: 2, key: 'key2'}, {button: 3, key: 'key1'}]);
  });

});
