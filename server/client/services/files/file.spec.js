'use strict';

describe('Service: Files', function () {

  beforeEach(module('soundboard'));

  var File, $httpBackend;

  beforeEach(inject(function (_File_, _$httpBackend_) {
    File = _File_;
    $httpBackend = _$httpBackend_;
  }));

  it('should get files', function () {
    $httpBackend.expectGET('filse.json').respond(['one', 'two', 'three']);

    File.getFiles().then(function(files) {
      expect(files.length).toBe(3);
      expect(files).toEqual(['one', 'two', 'three']);
    });
  });

});
