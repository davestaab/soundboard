'use strict';

angular.module('soundboard')
.service('File', function ($http) {
  var service = this;

  service.getFiles = function() {
    return $http.get('files.json').then(function(response) {
      return response.data;
    });
  };
});
