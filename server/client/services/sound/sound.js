'use strict';

angular.module('soundboard')
  .service('Sound', function ($http) {

    var service = this;

    service.getSounds = function() {
      return $http.get('sounds.json').then(function(response) {
        return response.data;
      });
    }

    service.saveSounds = function(sounds) {
      return $http.post('sounds.json', sounds).then(function(response) {
        return response.data;
      });
    }


  });
