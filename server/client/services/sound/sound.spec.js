'use strict';

describe('Service: Sound', function () {

  beforeEach(module('soundboard'));

  var Sound, $httpBackend;

  beforeEach(inject(function (_Sound_, _$httpBackend_) {
    Sound = _Sound_;
    $httpBackend = _$httpBackend_;
  }));

  it('should get sounds', function () {
    $httpBackend.expectGET('sounds.json').respond({pin12: { sounds: ['homer.wav'], button: 1}});
    Sound.getSounds().then(function(sounds) {
      expect(sounds.pin12.sounds.length).toBe(1);
    });
  });

  it('should save sounds');

});
