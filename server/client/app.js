'use strict';

angular.module('soundboard', [
  'ngRoute',
  'ngMaterial'
])
  .config(function ($routeProvider, $locationProvider, $mdThemingProvider) {
    
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);

  });
