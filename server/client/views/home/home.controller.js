'use strict';

angular.module('soundboard')
  .controller('HomeCtrl', function (Sound, File) {

    var vm = this;

    function loadSounds() {
      Sound.getSounds().then(function(sounds) {
        vm.sounds = sounds;
      });
    };

    loadSounds();

    File.getFiles().then(function(files){
      vm.files = files;
    });



    vm.querySearch = function(query) {
      // console.log(query, vm.files);
      var results = query ? vm.files.filter( createFilterFor(query) ) : vm.files;
      console.log('results for ', query, results);
      return results;
    }

    vm.save = function(sounds) {
      Sound.saveSounds(sounds).then(function(data) {
        console.log('sounds saved!');
      });
    }

    vm.reset = function() {
      loadSounds();
    }


    /**
    * Create filter function for a query string
    */
   function createFilterFor(query) {
     console.log(query);
     var lowercaseQuery = angular.lowercase(query);
     return function filterFn(file) {
      //  console.log('file, query', file, lowercaseQuery);
       return (file.indexOf(lowercaseQuery) !== -1);
     };
   }

  });
