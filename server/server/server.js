'use strict';

var express = require('express');
var config = require('./config/environment');

var app = express();
var server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port, config.ip, function () {

  console.log(
    '\nExpress server listening on port '
    + config.port
    + ', in '
    + app.get('env')
    + ' mode.\n'
  );

  // if (config.env === 'development') {
  //   // require('ripe').ready();
  // }

});

module.exports = server;
