'use strict';

var path = require('path');
var merge = require('lodash.merge');

var all = {

  env: process.env.NODE_ENV || 'development',
  root: path.normalize(__dirname + '/../../..'),
  port: process.env.PORT || 9000,
  soundDir: '../sounds/'
};

module.exports = merge(all, require('./' + all.env + '.js'));
