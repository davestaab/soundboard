'use strict';
var jsonfile = require('jsonfile')
var config = require('./config/environment');
var glob = require('glob');

module.exports = function (app) {

  // API

  app.get('/sounds.json', function(req, res) {
    jsonfile.readFile('../sounds.json', function(err, obj) {
      if(err) {
        console.log(err);
        res.status(500).end();
      }
      res.json(obj).end();
    });
  });

  app.get('/files.json', function(req, res) {
    glob(config.soundDir + "*.wav", function (err, files) {
      if(err) {
        console.log(err);
        res.status(500).end();
      }
      res.json(files.map(function(file){
        return file.replace(config.soundDir, '');
      }));
    })
  });


  app.post('/sounds.json', function(req, res, next){
    console.log('saving sounds', req.body);
    jsonfile.writeFile('../sounds.json', req.body, function(err) {
      if(err) {
        console.log(err);
        res.status(400).send('Bad Request');
      }
      console.log('saved sounds!');
      res.end();
    });
  });

  app.route('/:url(api|app|bower_components|assets)/*')
  .get(function (req, res) {
    res.status(404).end();
  });

  app.route('/*')
  .get(function (req, res) {
    res.sendFile(
      app.get('appPath') + '/index.html',
      { root: config.root }
    );
  });
};
