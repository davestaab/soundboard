# Soundboard

Software to run a soundboard on a raspberry pi. 

## Hardware tutorial

Here is the starting point for the hardware tutorial. 

http://makezine.com/projects/make-33/simple-soundboard/

## converting sound files

These are useful tools and things to know when working with sounds.

### Using mpg123
On mac: `brew install mpg123`

'mpg123 --wav homer_award-2.wav --8bit --rate 8000 --mono homer_award.wav'

The --8bit options forces 8 bit audio. It could be dropped if it reduces sound quality.

## configure sound out

The [docs](https://www.raspberrypi.org/documentation/configuration/audio-config.md)

`amixer cset numid=3 1`

2 = HDMI, 1 = analogue, 0 = auto

## View file encoding

Use `file <file.wav>` to see what encoding and format the sound file is in.

```
> file squirrel_1.3.wav
squirrel_1.3.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, stereo 44100 Hz
```

## Starting soundboard.py

``` sudo ./soundboard.py ```

# Setup node server
Installation is only required for a new install. The **pi** is already setup so just start the server.

## Installing Server Dependencies

```
cd server
npm install --production
bower install --production
```

## Starting server

```
cd server
npm start
```

## Sound ideas

1. I love it when a plan comes together (victory)
1. Braveheart freedom (victory)
1. hyper space jump fail from star wars (no)
1. yoohuuu big summer blowout - frozen (distraction)
1. "ooh, piece of candy" - family guy (distraction)
1. Does that make you feel in control - Bane from Batman
1. Now his failure is complete - Darth Vader
1. price is right failure horn (defeat)
1. ~~"no no no no no" - Michael Scott (no)~~
1. ~~zelda puzzle solve (victory)~~
1. ~~zelda item find (victory)~~
1. ~~Hallelujah chorus~~
1. ~~Phantom of the opera~~
1. ~~Make it so number 1 - Picard (yes)~~